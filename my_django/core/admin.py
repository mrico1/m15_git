from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as Admin
from django.utils.translation import gettext_lazy as _
from core import models

class UserAdmin(Admin):

    ordering = ['id']
    list_display = ['email', 'name', 'is_active']
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (_("Personal info"), {"fields": ("name",)}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login",)}),
    )

admin.site.register(models.User, UserAdmin)
admin.site.register(models.Device)
