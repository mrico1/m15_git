from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.conf import settings
#comentario

class UsersManagers(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        """
        Crea un nou usuari
        :param email: correu electronic
        :param psswd: contrasenya
        :param extra_fields: camps extra
        :return: usuari identificat pel seu mail
        """
        if not email:
            raise ValueError('Error. El camp email ha de tenir valor')
        usuari = self.model(email=self.normalize_email(email), **extra_fields)
        usuari.set_password(password)
        usuari.save(using=self._db)
        return usuari

    def create_superuser(self, email, password=None):

        usuari = self.create_user(email,password=password)
        usuari.is_superuser = True
        usuari.is_staff = True
        usuari.save(using=self._db)
        return usuari


class User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_banned = models.BooleanField(default=False)

    objects = UsersManagers()

    USERNAME_FIELD = 'email'

class Device(models.Model):
    mac = models.CharField(max_length=32, null=False)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    name = models.CharField(max_length=32, null=True)
    latt = models.DecimalField(max_digits=8, decimal_places=6, blank=True, null=True)
    long = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    def __str__(self):
        return f'{self.mac} : {self.name} : {self.latt} : {self.long}'