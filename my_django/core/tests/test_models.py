from django.db import IntegrityError
from django.test import TestCase
from django.contrib.auth import get_user_model
from core import models

def usuari_exemple(email='sample@ies-eugeni.cat', password='1234p'):
    return get_user_model().objects.create_user(
            email=email,
            password=password,
        )

class Testmodels(TestCase):


    def test_crea_usuari_amb_email(self):
        """
        Test para crear usuario con un Mail

        :return:
        """

        email = 'sample@sample.cat'
        psswd = '1234'
        usuari = get_user_model().objects.create_user(
            email=email,
            password=psswd,
            name='Malario'
        )
        usuari.save()
        self.assertEqual(usuari.email, email)
        self.assertTrue(usuari.check_password(psswd))

    def test_comprova_email_duplicat(self):
        """
        Test comprovar si l'usuari ja existeix
        :return:
        """
        with self.assertRaises(IntegrityError):
            u = get_user_model().objects.create_user(
                    "sample@sample.cat",
                    password='contra'
                )
            u = get_user_model().objects.create_user(
                    "sample@sample.cat",
                    password='contra'
                )


    def test_normalitza_mail(self):
        """
        Comprova mail normailitzat
        :return:
        """
        email = 'oasjdff@knoASIUOF.cat'
        psswd = 'afdjoasd'
        usuario = get_user_model().objects.create_user(
            email=email,
            password=psswd
        )
        self.assertEqual(usuario.email, email.lower())

    def test_crea_usuari_amb_mail_incorrecte(self):
        with self.assertRaises(ValueError):
            u = get_user_model().objects.create_user(None, 'pssd')

    def test_crea_nou_dispositiu(self):
        mac = '000-000-000'
        dev = models.Device.objects.create(
            user=usuari_exemple(),
            mac=mac
        )

        self.assertEqual(dev.mac, mac)